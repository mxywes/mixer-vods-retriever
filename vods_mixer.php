<head><link rel="icon" type="img/ico" href="/../favicon2.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600|Source+Code+Pro'>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


<?php
$channel = $url = null;
function var_dump_pre($mixed = null) {
  echo '<pre>';
  var_dump($mixed);
  echo '</pre>';
  return null;
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
setlocale(LC_TIME, "fr_FR");
function file_get_contents_curl($url) {
$ch = curl_init();

curl_setopt_array($ch, array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => $url
 ));
 $response = curl_exec($ch);
 $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
 echo curl_error($ch);
 curl_close($ch);

return ($httpcode>=200 && $httpcode<300) ? $response : false;
}
//if ($_SERVER["REQUEST_METHOD"] == "GET") {

if (isset($_REQUEST["channel"])) {
	$channel = test_input($_REQUEST["channel"]);

echo $channel;
$array = json_decode(file_get_contents_curl('https://mixer.com/api/v1/channels/' . $channel), true);
if (!isset($array)) {echo ('<pre>Channel not found, returned an error.</pre>');}
else {
extract($array, EXTR_OVERWRITE);
echo ('<pre>Retrieved channel ID: '. $id . '</pre>');
$url = 'https://mixer.com/api/v1/channels/' . $id . '/recordings?order=id:DESC';
}
}
?>
<title><?php echo($token)?> VODs on Mixer</title></head>
<body>
<form action="" method="GET" id="form"><input type="text" name="channel" value="<?php if(isset($_REQUEST["channel"])){echo($_REQUEST["channel"]);};?>" onchange="this.form.submit()"/></form>
Click the thumbnail to copy the VOD URL to the clipboard; and on any text line to open the player.

<div class="Tiles">
<?php
if (isset($url)) {
$json_array = json_decode(file_get_contents_curl($url), true);
extract($json_array, EXTR_OVERWRITE);


for($i = 0, $size = count($json_array); $i < $size; ++$i) {
	extract($json_array[$i], EXTR_OVERWRITE);
	echo('<div class="Tile js-tile">
    <div class="Tile-content Tile-content--toggle js-toggle-tile"
      role="button"
      tabindex="0">
      <div class="thumb" onclick="copyToClipboard(\'url' . $i . '\')">
        <img src="' . $vods[0]['baseUrl'] . 'source.png	" alt="">
      </div>
      <p class="title" onclick="window.open(\'player.php?video=' . $vods[0]['baseUrl'] . 'manifest.m3u8&id=' . $id . '&title=' . $name . '\')">' . $name . '</p>
      <p class="details" onclick="window.open(\'player.php?video=' . $vods[0]['baseUrl'] . 'manifest.m3u8&id=' . $id . '&title=' . $name . '\')">' . ucwords(strftime("%A %x %H:%M:%S", strtotime($createdAt))) . '</p>
	  <p class="details" onclick="window.open(\'player.php?video=' . $vods[0]['baseUrl'] . 'manifest.m3u8&id=' . $id . '&title=' . $name . '\')">' . date('H:i:s', mktime(0, 0, $duration)) . '</p>
	  <p class="details" onclick="window.open(\'player.php?video=' . $vods[0]['baseUrl'] . 'manifest.m3u8&id=' . $id . '&title=' . $name . '\')">' . $vods[0]['data']['Width'] . 'x' . $vods[0]['data']['Height'] . ' ' . round($vods[0]['data']['Fps'], 2, PHP_ROUND_HALF_ODD) . 'fps</p>
	  <p class="url" id="url' . $i . '" onclick="window.open(\'player.php?video=' . $vods[0]['baseUrl'] . 'manifest.m3u8&id=' . $id . '&title=' . $name . '\')">' . $vods[0]['baseUrl'] . 'manifest.m3u8</p>
    </div>
  </div>');
}
}
 ?>
</div>
</body>
<script>
function copyToClipboard(elementId) {

  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value", document.getElementById(elementId).innerHTML);

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);

}
</script>